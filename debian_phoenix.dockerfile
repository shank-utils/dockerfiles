# use the official elixir image
FROM elixir:1.7

LABEL maintainer="shashanksharma21@gmail.com"


# Important!  Update this no-op ENV variable when this Dockerfile
# is updated with the current date. It will force refresh of all
# of the base images and things like `apt-get update` won't be using
# old cached versions when the Dockerfile is built.
ENV ELIXIR_VERSION=v1.7.2 \
    NODEJS_VERSION=v8.11.3

ARG DEBIAN_FRONTEND=noninteractive

# install requisites
# update package list
RUN apt-get update && \
    # install apt-utils
    apt-get install -y --no-install-recommends apt-utils && \
    # tools
    apt-get install -y --no-install-recommends git build-essential inotify-tools && \
    # nodejs
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y --no-install-recommends nodejs && \
    # postgresql-client
    echo 'deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main' > /etc/apt/sources.list.d/pgdg.list && \
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - && \
    apt-get update && \
    apt-get install -y --no-install-recommends postgresql-client-10 && \
    # cleanup
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*

# Install hex (Elixir package manager) and rebar (Erlang build tool)
RUN mix local.hex --force && \
    mix local.rebar --force

# Install Phoenix
RUN mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phoenix_new.ez

CMD [ "/bin/bash" ]
