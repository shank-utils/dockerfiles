FROM elixir:1.7-alpine

LABEL maintainer="shashanksharma21@gmail.com"

# Important!  Update this no-op ENV variable when this Dockerfile
# is updated with the current date. It will force refresh of all
# of the base images and things like `apt-get update` won't be using
# old cached versions when the Dockerfile is built.
ENV ELIXIR_VERSION=v1.7.2 \
    NODEJS_VERSION=v8.11.3

RUN apk --no-cache update && apk upgrade
RUN apk --no-cache add inotify-tools
RUN apk --no-cache add postgresql-dev
RUN apk --no-cache add nodejs npm

RUN mix local.hex --force
RUN mix local.rebar --force

RUN mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phoenix_new.ez